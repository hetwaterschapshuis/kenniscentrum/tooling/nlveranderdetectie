"""Setup the package."""
from setuptools import setup

# Version number

import re
VERSIONFILE = "nlveranderdetectie/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RuntimeError(f"Unable to find version string in {VERSIONFILE}.")


with open("README.md", "r") as fh:
    long_description = fh.read()

# Requirements are empty as you should use the environment.yml file to install dependencies instead.
requirements = []

setup(
    name="nlveranderdetectie",
    version=verstr,
    author="Jonathan Gerbscheid",
    author_email="j.gerbscheid@hetwaterschapshuis.nl",
    description="Mutatiedetectie voor BGT op basis van luchtfoto's en image segmentation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hetwaterschapshuis/kenniscentrum/tooling/nlveranderdetectie",
    packages=['nlveranderdetectie'],
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: All rights reserved",
        "Operating System :: OS Independent",
        'Development Status :: 3 - Alpha'
    ],
    python_requires=">=3.10",
    entry_points={
        "console_scripts" : ['nlver=nlveranderdetectie:main']
    }
)
