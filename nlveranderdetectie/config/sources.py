DATA_URLS = {
    "cir": {
        "url": "https://service.pdok.nl/hwh/luchtfotocir/wms/v1_0?request=GetCapabilities&service=wms",
        "layers": {
            "2017": "2017_ortho25IR",
            "2018": "2018_ortho25IR",
            "2019": "2019_ortho25IR",
            "2020": "2020_ortho25IR",
            "current": "Actueel_ortho25IR"
        },
        "layer_resolution": {
            "2017": (4000, 4000),
            "2018": (4000, 4000),
            "2019": (4000, 4000),
            "2020": (4000, 4000),
            "current": (4000, 4000)
        }
    },
    "ahn3": {
        "url": "https://geodata.nationaalgeoregister.nl/ahn3/wms?request=GetCapabilities&service=wms",
        "layers": {
            "05m_dsm": "ahn3_05m_dsm",
            "05m_dsm": "ahn3_05m_dsm",
            "5m_dtm": "ahn3_5m_dtm",
            "5m_dtm": "ahn3_5m_dtm",
        },
        "layer_resolution": {
            "05m_dsm": (2000, 2000),
            "05m_dsm": (2000, 2000),
            "5m_dtm": (200, 200),
            "5m_dtm": (200, 200),
        }
    },
    "rgb": {
        "url": "https://service.pdok.nl/hwh/luchtfotorgb/wms/v1_0?request=GetCapabilities&service=wms",
        "layers": {
            "2016": "2016_ortho25",
            "2017": "2017_ortho25",
            "2018": "2018_ortho25",
            "2019": "2019_ortho25",
            "2020": "2020_ortho25",
            "2021": "2021_orthoHR",
            "2021_10cm": "2021_orthoHR",
            "current": "Actueel_orthoHR"
        },
        "layer_resolution": {
            "2016": (4000, 4000),
            "2017": (4000, 4000),
            "2018": (4000, 4000),
            "2019": (4000, 4000),
            "2020": (4000, 4000),
            "2021": (12500, 12500),
            "2021_10cm": (10000, 10000),
            "current": (12500, 12500)
        }
    },
    "bgt": {
        "url": "https://api.pdok.nl/lv/bgt/download/v1_0/full/custom",
        "layers": [],
        "layer_resolution": {
            "25cm": (4000, 4000),
            "10cm": (10000, 10000),
            "8cm": (12500, 12500)
        }
    }
}


# coordinates in RD (1=1km scale) for various relevant areas
REGION_DICT = {
    "anm_poly": [131, 370, 201, 427],
    "anm": [131, 370, 201, 427],
    "zwolle": [200, 500, 205, 505],
    "demo": [94, 466, 96, 468],
    "wbd": [71, 374, 136, 416],
    "hdsr": [109, 438, 170, 468],
    "hhnk_demo": [114, 516, 119, 522],
    "hhd": [65, 433, 100, 461],
    "vijfheerenlanden": [133, 442, 137, 445],
    "wdod": [181, 471, 247, 561],
    "wfry": [152, 535, 225, 603],
    "ahn4_demo": [120, 475, 130, 487],
    "pnhdemo": [115, 474, 116, 475],
    "pnhvalid": [107, 486, 110, 488],
    "pnhvalid_laak": [85, 452, 86, 454],
    "pnh": [81, 466, 150, 565],
    "bgt_groningen": [230, 578, 235, 582],
    "bgt_friesland": [167, 545, 206, 588],
    "bgt_drenthe": [229, 553, 233, 557],
    "bgt_overijssel": [237, 486, 241, 490],
    "bgt_flevoland": [159, 500, 163, 504],
    "bgt_gelderland": [187, 439, 191, 443],
    "bgt_utrecht": [138, 455, 142, 459],
    "bgt_noordholland": [101, 498, 105, 502],
    "bgt_zuidholland": [71, 446, 75, 450],
    "bgt_zeeland": [33, 390, 37, 394],
    "bgt_brabant": [145, 412, 149, 416],
    "bgt_limburg": [190, 356, 194, 360],
    "fryslan_pilot1": [185, 563, 195, 573],
    "aa_en_maas_pilot1": [153, 414, 163, 425],
    "aa_en_maas_pilot2": [185, 397, 195, 408],
    "hdsr_sample": [119, 455, 134, 468],
    "test_bgt":[211,  473, 212,474]
}
