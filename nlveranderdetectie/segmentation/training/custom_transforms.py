import numpy as np
import random
import torch
from scipy.ndimage import rotate as scipyrotate

class NormalizeNumpy(object):
    """normalize block with given means and stds, expects a numpy array."""

    def __init__(self, means, stds):
        self.means = means
        self.stds = stds

    def __call__(self, sample):
        img = sample['image']
        img = img.astype(np.float32)

        # normalize the input data
        for i in range(len(self.means)):
            img[:, :, i] = (img[:, :, i] - self.means[i]) / self.stds[i]
        sample['image'] = img
        return sample

class FlipChannelNumpy(object):
    """swap first and last channel in a 3d numpy array."""

    def __call__(self, sample):
        image = sample['image']
        sample['image'] = np.moveaxis(image, -1, 0)
        return sample

class RandomHorizontalFlipNumpy(object):
    def __call__(self, sample):
        img = sample['image']
        mask = sample['label']
        if random.random() < 0.5:
            img = np.flip(img.copy(), axis=1)
            mask = np.flip(mask.copy(), axis=1)

        return {'image': img,
                'label': mask}

class RandomVerticalFlipNumpy(object):
    def __call__(self, sample):
        img = sample['image']
        mask = sample['label']
        if random.random() < 0.5:
            img = np.flip(img.copy(), axis=0)
            mask = np.flip(mask.copy(), axis=0)

        return {'image': img,
                'label': mask}

class RandomRotate90Numpy(object):
    """Randomly rotate a sample in 90 degree intervals.

    Args:
        sample (np.array): array to rotate
    
    Returns:
        np.array: rotated array.
    """
    def __call__(self, sample):
        img = sample['image']
        mask = sample['label']
        n_rot = np.random.randint(1, 4)
        if random.random() < 0.5:
            img = np.rot90(img.copy(), k=n_rot, axes=(0, 1))
            mask = np.rot90(mask.copy(), k=n_rot, axes=(0, 1))
        return {'image': img,
                'label': mask}

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""
    def __call__(self, sample):
        img = sample['image']
        mask = sample['label']
        img = torch.from_numpy(img.copy()).float()
        mask = torch.from_numpy(mask.copy()).float()
        return {'image': img,
                'label': mask}

class ToNumpy(object):
    """Convert ndarrays in sample to Tensors."""
    def __call__(self, sample):
        img = sample['image']
        mask = sample['label']
        img = np.array(img.copy(), dtype=np.float32)
        mask = np.array(mask.copy(), dtype=np.float32)
        return {'image': img,
                'label': mask}

# TODO: check if this function works as it should, dont use it for now.
class RandomRotateNumpy(object):
    def __init__(self, degree):
        self.degree = degree

    def __call__(self, sample):
        img = sample['image']
        mask = sample['label']
        rotate_degree = random.uniform(-1 * self.degree, self.degree)
        rotated = []
        for i in range(img.shape[-1]):
            img = scipyrotate(img[:, :, i], rotate_degree, mode='reflect', reshape=False)
            rotated.append(img)

        img = np.stack(rotated, axis=-1)
        mask = scipyrotate(mask, rotate_degree, mode='reflect', reshape=False, order=0)

        return {'image': img,
                'label': mask}

class ClipAHN(object):
    """clip values for dtm and dsm channels, expects a numpy array"""
    
    def __init__(self, dsmchannel, dtmchannel, lowclip=-25, highclip=25):
        self.dsmchannel = dsmchannel
        self.dtmchannel = dtmchannel

    def __call__(self, sample):
        image = sample['image']
        
        # nans to -25, nans should be at -1000 + random noise, meaning they should get captured in the 
        # < -500 statement
        image[:, :, self.dsmchannel][image[:, :, self.dsmchannel] < self.lowclip] = self.lowclip
        image[:, :, self.dtmchannel][image[:, :, self.dtmchannel] < self.lowclip] = self.lowclip

        image[:, :, self.dsmchannel][image[:, :, self.dsmchannel] > self.highclip] = self.highclip
        image[:, :, self.dtmchannel][image[:, :, self.dtmchannel] > self.highclip] = self.highclip
                          
        sample['image'] = image
        return sample

class MedianHeightNormalize(object):
    """move all values that are not extremely low (and thus probably nans) to a normalized level"""
    
    def __init__(self, dtmchannel, dsmchannel, height_norm_val):
        self.dsmchannel = dsmchannel
        self.dtmchannel = dtmchannel
        self.height_norm_val = height_norm_val

    def __call__(self, sample):
        image = sample['image']

        # move all non-nan values to a normalized ground level
        image[:, :, self.dsmchannel][image[:, :, self.dsmchannel] > -100] -= self.dtm_median
        image[:, :, self.dtmchannel][image[:, :, self.dtmchannel] > -100] -= self.dtm_median
                          
        sample['image'] = image
        return sample

class AddHeightNoise(object):
    """add noise to the dsm and dtm channels"""
    
    def __init__(self, height_range, dtmchannel, dsmchannel):
        self.height_range = height_range
        self.dsmchannel = dsmchannel
        self.dtmchannel = dtmchannel

    def __call__(self, sample):
        image = sample['image']
        noise = np.random.uniform(-self.height_range, self.height_range)
        
        image[:, :, self.dsmchannel] += noise
        image[:, :, self.dtmchannel] += noise      
                          
        sample['image'] = image
        return sample