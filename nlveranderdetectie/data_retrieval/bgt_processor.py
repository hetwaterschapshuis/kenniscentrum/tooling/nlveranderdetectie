#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
import subprocess
from multiprocessing import Pool
from pathlib import Path
from random import shuffle

import tqdm


def make_clip_call(xmin, ymin, xmax, ymax, targetfile, sourcefile):
    # (xmin, ymin, xmax, ymax, targetfile, sourcefile) = args
    command = [
        "ogr2ogr",
        "-s_srs",
        "EPSG:28992",
        "-t_srs",
        "EPSG:28992",
        "-spat",
        xmin,
        ymin,
        xmax,
        ymax,
        "-f",
        "ESRI Shapefile",
        targetfile,
        sourcefile,
    ]
    if (not os.path.exists(targetfile)) and (os.path.exists(sourcefile)):
        process = subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        process.wait()
        # stdout, stderr = process.communicate()

def make_rasterize_call(xmin, ymin, xmax, ymax, targetfile, sourcefile):
    command = [
        "gdal_rasterize",
        "-burn",
        "1",
        "-ot",
        "Byte",
        "-co",
        "COMPRESS=LZW",
        "-co",
        "NUM_THREADS=ALL_CPUS",
        "-co",
        "PREDICTOR=1",
        '-co', 
        'TILED=YES',
        "-ts",
        "12500",
        "12500",
        "-te",
        xmin,
        ymin,
        xmax,
        ymax,
        sourcefile,
        targetfile,
    ]
    if (not os.path.exists(targetfile)) and (os.path.exists(sourcefile)):
        process = subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        process.wait()
        # stdout, stderr = process.communicate()
    return None

    
    
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Clip and rasterize shapefiles into grid tiles for training/segmentation')
    parser.add_argument('--source-dir',
                        default=r"D:\beeldmateriaal\bgt\bgt-gmllight-nl-nopbp-shp",
                        help='directory containing source shapefiles',
                        type=str)
    parser.add_argument('--target-dir',
                        default=r"D:\beeldmateriaal\bgt\tmp\shapefile_tiles",
                        help='where to save output.',
                        type=str)
    args = parser.parse_args()
    
    # get source shapefiles
    SOURCE_SHAPEFILE_PATHS = list(Path(args.source_dir).glob("*.shp"))
    SOURCE_SHAPEFILE_PATHS = dict.fromkeys(SOURCE_SHAPEFILE_PATHS)
    for path in SOURCE_SHAPEFILE_PATHS.keys():
        SOURCE_SHAPEFILE_PATHS[path] = path.split("\\")[-1].split(".")[0]
        
    print(f"Found {len(SOURCE_SHAPEFILE_PATHS)} source shapefiles")
    # SOURCE_SHAPEFILE_PATHS = {'D:\\beeldmateriaal\\bgt\\bgt-gmllight-nl-nopbp-shp\\wegdeel.shp': 'wegdeel'}
    for fp in SOURCE_SHAPEFILE_PATHS:
        print(f"    - {fp}")

    
    # extents for NL
    # xmin_full = 0
    # xmax_full = 300000
    # ymin_full = 289000
    # ymax_full = 629000

    # extents for fryslan
    # 152, 535, 225, 603
    xmin_full = 152000
    xmax_full = 225000
    ymin_full = 535000
    ymax_full = 603000

    xmin_full_km = round(xmin_full / 1000)
    xmax_full_km = round(xmax_full / 1000)
    ymin_full_km = round(ymin_full / 1000)
    ymax_full_km = round(ymax_full / 1000)


    print(f"using RD extends: xmin={xmin_full_km}, xmax={xmax_full_km}, ymin={ymin_full_km}, ymax={ymax_full_km}")

    nl_coords_km = []
    nl_coords_m = []
    for x in tqdm.tqdm(range(xmin_full_km, xmax_full_km), desc="sweeping coordinates"):
        for y in range(ymin_full_km, ymax_full_km):
            nl_coords_km.append((x, y, x + 1, y + 1))
            nl_coords_m.append((x * 1000, y * 1000, (x + 1) * 1000, (y + 1) * 1000))

    print(f"number of coordinate blocks: {len(nl_coords_m)}")
    shuffle(nl_coords_m)
    shuffle(nl_coords_km)
    print(f"example coordinate in RD_new/meters: {nl_coords_m[0]}")

    # make argument list for gdal clip calls
    argument_list_clip = []
    for key, value in SOURCE_SHAPEFILE_PATHS.items():
        base_shapefile = key
        target_basepath_shp = Path(args.target_dir).joinpath('shapefile_tiles', value)
        target_basepath_raster = Path(args.target_dir).joinpath('raster_tiles', value)
        os.makedirs(target_basepath_shp, exist_ok=True)
        os.makedirs(target_basepath_raster, exist_ok=True)

        argument_list_clip = argument_list_clip + [
            (
                str(xmin),
                str(ymin),
                str(xmax),
                str(ymax),
                os.path.join(target_basepath_shp, f"{xmin}_{ymax}.shp"),
                base_shapefile,
            )
            for (xmin, ymin, xmax, ymax) in nl_coords_m
        ]

    # shuffle for better estimate of performance
    shuffle(argument_list_clip)

    # make argument list for gdal rasterization calls
    argument_list_raster = [
        (
            xmin,
            ymin,
            xmax,
            ymax,
            targetfile.replace(".shp", ".tif").replace("shapefile_tiles", "raster_tiles"),
            targetfile,
        )
        for (xmin, ymin, xmax, ymax, targetfile, sourcefile) in argument_list_clip
    ]

    # argument_list_clip = argument_list_clip[:100]
    # res = thread_map(make_clip_call, argument_list_clip, max_workers=32, chunksize=5)
    print("parallel processing of clip calls")
    
    with Pool(processes=30) as pool:
        res = pool.starmap(make_clip_call, tqdm.tqdm(argument_list_clip[:2]), chunksize=1)
        
    with Pool(processes=30) as pool:
        res = pool.starmap(make_rasterize_call, tqdm.tqdm(argument_list_raster), chunksize=1)
